/*
 * MO_PSOPruner_Pareto.cpp
 *
 *  Created on: Aug 16, 2011
 *      Author: Rob
 */

#include "MO_PSOPruner_Pareto.h"

MO_PSOPruner_Pareto::MO_PSOPruner_Pareto(int popSize, int generations, Classifier* o, std::vector<Generator> g, std::vector<Line> l, double p, bool ul)
		 : MO_PSOPruner(popSize, generations, o, g, l, p, ul){
	Init(popSize, generations, o, g, l, p);

	cFactor = 1;
}

MO_PSOPruner_Pareto::~MO_PSOPruner_Pareto() {
	// TODO Auto-generated destructor stub
}
void MO_PSOPruner_Pareto::initPopulation(MTRand& mt){

	swarm.clear(); gBest.clear(); gBestValues.clear();

	for(int i=0; i<Np; i++){
		MO_ParetoBinaryParticle p;

		p.pBest.resize(Nd,0);
		
		#ifdef _MSC_VER
			p.pBestValues.push_back(-std::numeric_limits<double>::infinity()); // Max
			p.pBestValues.push_back(-std::numeric_limits<double>::infinity()); // Max
		#else
			p.pBestValues.push_back(-INFINITY); // Max
			p.pBestValues.push_back(-INFINITY); // Max
		#endif

		for(int j=0; j<(int)gens.size(); j++){
			p.pos.push_back((mt.rand() < gens[j].getOutageRate() ? 0 : 1 ));
			p.vel.push_back(gens[j].getOutageRate());
		}
		if(useLines && lines.size() > 0){
			int linesDown = 0;
			for(int j=0; j<(int)lines.size(); j++){
				p.pos.push_back(1);
				if(p.pos[j] == 0) linesDown++;
				p.vel.push_back(lines[j].getOutageRate());
			}
		}
		swarm.push_back(p);
	}
}
void MO_PSOPruner_Pareto::evaluateFitness(MTRand& mt){
	std::vector<double> results;
	for(unsigned int i=0; i<swarm.size(); i++){

		results = EvaluateSolution(swarm[i].pos);
		swarm[i].fitness.clear();

		results[1] = results[2]-results[1];
		swarm[i].fitness = results;

		//Local Best
		// If it dominates the current local best
		if(results[0] > swarm[i].pBestValues[0] && results[1] > swarm[i].pBestValues[1]){
			swarm[i].pBest      	= swarm[i].pos;
			swarm[i].pBestValues[0] = results[0];
			swarm[i].pBestValues[1] = results[1];
		}
	}
}
void MO_PSOPruner_Pareto::Prune(MTRand& mt){

	timer.startTimer();
	initPopulation(mt);
	totalIterations = 0;

	while(!MO_PSOPruner::isConverged()){
		evaluateFitness(mt);
		updateArchive(mt);
		chooseLeader(mt);
		updatePositions(mt);
		totalIterations++;
	}
	timer.stopTimer();
	pruningTime = timer.getElapsedTime();
}

void MO_PSOPruner_Pareto::updateArchive(MTRand& mt){
	bool dominated;
	std::vector < std::vector < double > > temp1;
	std::vector < std::vector < int > > temp2;

	for(unsigned int i=0; i<swarm.size(); i++){
		dominated = false;
		for(unsigned int j=0; j<swarm.size(); j++){
			//If F(i) < F(j), then j dominates i
			if(swarm[i].fitness[0] < swarm[j].fitness[0] && swarm[i].fitness[1] < swarm[j].fitness[1]){
				dominated = true;
				break;
			}
		}
		if(!dominated){
			// Is it truly  non-dominated?
			for(unsigned int m=0; m<archivedFitness.size(); m++){
				if(swarm[i].fitness[0] < archivedFitness[m][0] && swarm[i].fitness[1] < archivedFitness[m][1]){
					dominated = true;
					break;
				}
			}

			if(!dominated){
				archivedFitness.push_back(swarm[i].fitness);
				archivedPosition.push_back(swarm[i].pos);
			}
		}
		//Clean Archive
		temp1.clear(); temp2.clear();
		for(unsigned int m=0; m<archivedFitness.size(); m++){
			dominated = false;
			for(unsigned int n=0; n<archivedFitness.size(); n++){
				if(archivedFitness[m][0] < archivedFitness[n][0] && archivedFitness[m][1] < archivedFitness[n][1]){
					dominated = true;
					break;
				}
			}
			if(!dominated){
				temp1.push_back(archivedFitness[m]);
				temp2.push_back(archivedPosition[m]);
			}
		}
		archivedFitness = temp1; archivedPosition = temp2;
	}

}
void MO_PSOPruner_Pareto::chooseLeader(MTRand& mt){
	//double niche = 0.5;
	double niche = 02;
	int total_loose = 0, add_loose = 0;
	std::vector<double> density(archivedFitness.size(), 0);
	//std::vector<double> distance(archivedFitness.size(), 0);
	double distance;
	std::vector<double> maxFitness(2, 0);
	std::vector<double> loose(archivedFitness.size(), 0);

	maxFitness[0] = 1.0; maxFitness[1] = 5.0;

	// Evaluate  Density
	for(unsigned int i=0; i<archivedFitness.size(); i++){
		for(unsigned int j=0; j<archivedFitness.size(); j++){
			distance = pow((archivedFitness[j][0]-archivedFitness[i][0])/maxFitness[0],2)+ pow((archivedFitness[j][1]-archivedFitness[i][1])/maxFitness[1],2);
			// TODO: may have to print these out to see how they work for scaling
			if(distance < niche){ density[i]++; }
		}
		loose[i] = 100.0/density[i];
		total_loose = total_loose + loose[i];
	}

	// Select Leader
	for(unsigned int i=0; i<swarm.size(); i++){
		swarm[i].leader = mt.randInt(archivedFitness.size()-1);
		add_loose = 0;
		for(unsigned int j=0; j<archivedFitness.size(); j++){
			add_loose += loose[i];
			if(add_loose/total_loose >= 1){
				swarm[i].leader = j;
				break;
			}
		}
	}
}
void MO_PSOPruner_Pareto::updatePositions(MTRand& mt){
	double R1, R2, newV;
	std::vector<double> gBest(2,0);

	for(unsigned int i=0; i<swarm.size(); i++){
		for(int j=0; j<Nd; j++){

			R1 	    = mt.rand();	R2 = mt.rand();
			gBest[0] = archivedPosition[swarm[i].leader][j];
			gBest[1] = archivedPosition[swarm[i].leader][j];

			newV =  W * swarm[i].vel[j];
			newV += C1 * R1 * (swarm[i].pBest[j] - swarm[i].pos[j]);
			newV += C2 * R2 * (gBest[j] 		 - swarm[i].pos[j]);
			newV = sigMoid(newV);

			swarm[i].vel[j] = newV;
			if(newV > mt.rand()){ swarm[i].pos[j] = 1;}
			else				{ swarm[i].pos[j] = 0;}
		}
	}
}

void MO_PSOPruner_Pareto::printArchive(){
	for(unsigned int i=0; i<archivedFitness.size(); i++){
		std::cout << std::setprecision(10) << archivedFitness[i][0]*100 << " " << archivedFitness[i][1] << std::endl;
	}
}
