/*
 * Chromosome.cpp
 *
 *  Created on: Nov 11, 2010
 *      Author: rgreen
 */

#include "Chromosome.h"

Chromosome::Chromosome() {
	// TODO Auto-generated constructor stub

}

Chromosome::Chromosome(int size) {
	pos.resize(size, 0);
}
Chromosome::~Chromosome() {
	// TODO Auto-generated destructor stub
}

std::string Chromosome::toString(){
	std::string s = "";
	for(int i=0; i<(int)pos.size(); i++){
		if(pos[i] == 1){s += "1";}
		else{s += "0";	}
	}
	return s;
}
