       #ifndef MCS_SAMPLING_H_
   #define MCS_SAMPLING_H_

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iterator>
#include <math.h>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>

#ifndef _OPENMP
    #include "omp.h"
#endif

#include "anyoption.h"
#include "Bus.h"
#include "defs.h"
#include "Generator.h"
#include "Line.h"
#include "MTRand.h"
#include "Primes.h"
#include "RandomNumbers.h"

namespace mcs_sampling{
    extern std::ifstream piFin;
    extern std::vector < std::vector < double > > sobol_points(unsigned N, unsigned D);
    extern double *single_sobol_points(unsigned N, unsigned D);

    extern std::vector < std::vector < double > > faureSampling(int Nd, int Ns);
    extern std::vector<int> changeBase(double num, double base, int numDigits);
    extern double corputBase(double base, double number);

    extern std::vector < std::vector < double > > latinHyperCube_Random(int numVars, int numSamples, MTRand& mt);
    extern std::vector < std::vector < double > > descriptiveSampling_Random(int numVars, int numSamples, MTRand& mt);

    extern std::vector < std::vector < double > > hammersleySampling(int Nd, int Ns);
    extern std::vector < std::vector < double > > haltonSampling(int Nd, int Ns);
    extern std::vector < std::vector < double > > faureSampling(int Nd, int Ns);

    extern double piNumber(int Ns);
    extern std::string toLower(std::string str);
    extern std::string toUpper(std::string str);
    extern std::vector<std::string> permuteCharacters(std::string topermute);
    extern std::string changeBase(std::string Base, int number);
}
#endif